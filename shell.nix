{ pkgs ? import <nixpkgs> { } }:
with pkgs;
mkShell rec {
  name = "truthmatters-env";

  nativeBuildInputs = [
  ];

  buildInputs = [
    hugo
    go
    nodejs
  ];
  
  shellHook = ''
  alias hss='hugo server --noHTTPCache'
  '';
}
