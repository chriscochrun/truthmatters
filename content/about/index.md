+++
title = "About me"

type = "blog"
+++

 Hi! My name is Dani Rouse and I grew up in rural northwest Kansas. Over the past few years, I have developed a deep passion to be educated and involved in politics. As an ordinary citizen, I was not sure how to get involved other than exercising my right to vote, but I knew I wanted to make an impact. Even deeper than my passion for politics is my passion for absolute truth. I am a Christian, and I whole-heartedly believe in absolute truth and its absolute necessity for a society to flourish- hence the name of the website. Protecting children from the broken ideologies overtaking our culture is of utmost importance to me, and my desire to get legislation developed to protect them kickstarted this website. My hope for this website is to inform people of important issues going on in our government, as well as empowering them to have a voice in what is going on by contacting their legislators. This site is brand new, so I hope to continue developing it as time goes on!

If you’d like to contact me with questions, concerns, or just to get to know more about what I am doing, my email is [truthmatters.cc@gmail.com](mailto:truthmatters.cc@gmail.com). 
