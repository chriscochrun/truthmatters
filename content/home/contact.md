---
widget: "contact_form"
title: "Send a letter to your legislator to join in the fight!" 

---

Want to get involved in the fight, but don’t know how as an ordinary citizen? Tell your legislators what matters to you! In signing and submitting the letter below, you can automatically send this letter via email to your district legislators telling them you care about the safety of Kansas children. 

Will you take a moment right now and tell your representatives to pass a bill protecting our children? 

Medical professionals around the nation are administering drugs and surgeries that are doing permanent, irreversible, and life-altering damage to our children. Physically healthy children are being sterilized and mutilated. Doctors are prescribing puberty-blockers that they claim are “reversible and harmless, like a pause button,” but these drugs are the same drugs used to chemically castrate convicted pedophiles. Then they perform surgeries removing healthy body parts that can cause immense health problems and can never be undone. And who is the biggest target of these horrors? Our children. 

Children do not have the mental development to make life-altering decisions, and it is our job as adults to protect them! Will you stand up for their safety and well-being? 

Let’s end child gender mutilation. Tell your representatives now. 

{{< spacer-line height="80px" >}}
# Join the fight to end this!

If you'd like to read the bill introduced, [click here](http://kslegislature.org/li/b2023_24/measures/documents/sb12_00_0000.pdf). 

Your information is not collected by this site at all. It is only sent on to the legislators for identification by them, but we do not collect any data whatsoever from you.
{{< form >}}

