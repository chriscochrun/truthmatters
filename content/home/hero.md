+++
widget = "hero"
# Order that this section will appear.
weight = 10

# Uncomment the following line and widget will NOT be displayed
# hidden = true

# Hero image (optional). Enter filename of an image in the `static/` folder.
# hero_media = "/img/idk.avif"

# Buttons
# Add as much button as you want
[[btn]]
	# The url of the button
  url = "about"
	# The text of the button
  label = "About Me"
	# Primary color
	primary = true

[[btn]]
  url = "#contact"
  label = 'Act Now'

+++

# End child **mutilation** in Kansas.

Learn more below and fill out the form to send a letter to your legislature and prevent doctors from "transitioning" minors.
